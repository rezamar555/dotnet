﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Matrices
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int euclidazo = Convert.ToInt32(textBox1.Text);
            int euclidazo2 = Convert.ToInt32(textBox2.Text);
            Euclidazo(ref euclidazo, euclidazo2);
           MessageBox.Show("El euclidazo es: "+ Euclidazo(ref euclidazo, euclidazo2));
        }

        public int Euclidazo(ref int a, int b)
        {
            if (b == 0)
                return a;
            else
                return Euclidazo(ref b, a % b);            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}
