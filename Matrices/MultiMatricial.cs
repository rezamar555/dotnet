﻿using System;
namespace Matrices
{
    public class MultiMatricial
    {
        
        public int[,] matriz, matri2, matri3; //A,B,C
        public int fil,col,fil2,col2,fil3,col3,total,opc; //Dimensiones y valores 
        public const char ast = '*'; // se usar para el muestreo

        public MultiMatricial()
        {

            do
            {
                Console.WriteLine("M U L T I P L I C A C I O N  D E  M A T R I C E S");
                Console.WriteLine("Como deseas crear la primer matriz?");
                Console.WriteLine("1.-Manual");
                Console.WriteLine("2.-Automatico");
                Console.WriteLine("3.-Regresar al menu principal");
                opc = Convert.ToInt32(Console.ReadLine());

                switch (opc)
                {
                    case 1:
                        Manual();
                        break;

                    case 2:
                        Autom();
                        break;

                }
            } while (opc != 2);

        }

        public void Manual()
        {
            Console.WriteLine("Indique el numero de filas: ");
            fil = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Indique el numero de columnas: ");
            col = Convert.ToInt32(Console.ReadLine());
            matriz = new int[fil+1, col+1];
            char[,] veces = new char[fil, col];
            Console.WriteLine("La matriz 1 es de: "+ fil+ "filas");
            Console.WriteLine("La matriz 1 es de: " + col + "columnas");
            for (int a = 0; a <= fil - 1; a++ )
            {
                for (int b = 0; b <= col- 1; b++)
                {
                    veces[a, b] = ast;
                }
            }
            for (int a = 0; a <= fil - 1; a++)
            {
                for (int b = 0; b <= col - 1; b++)
                {
                    Console.Write(" ["+ veces[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            Console.WriteLine("Prosiga con las filas de la matriz 2: ");
            fil2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Prosiga con las columnas de la matriz 2: ");
            col2 = Convert.ToInt32(Console.ReadLine());
            matri2 = new int[fil2+1, col2+1];
            char[,] veces2 = new char[fil2, col2];
            Console.WriteLine("La matriz 2 es de: " + fil2 + "filas");
            Console.WriteLine("La matriz 2 es de: " + col2 + "columnas");
            for (int a = 0; a <= fil2- 1; a++)
            {
                for (int b = 0; b <= col2 - 1; b++)
                {
                    veces2[a, b] = ast;
                }
            }
            for (int a = 0; a <= fil2 - 1; a++)
            {
                for (int b = 0; b <= col2 - 1; b++)
                {
                    Console.Write(" [" + veces2[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            Console.WriteLine("Ingrese los datos de la Matriz 1:");
            for (int a = 0; a <= fil -1; a++)
            {
                for (int b = 0; b <= col -1; b++)
                {
                    Console.Write("Ingrese la casilla ["+ a +"] "+"["+ b +"]");
                    matriz[a, b] = int.Parse(Console.ReadLine());
                }
                Console.WriteLine();
            }

            Console.WriteLine("Ingrese los datos de la Matriz 2:");
            for (int a = 0; a <= fil2 - 1; a++)
            {
                for (int b = 0; b <= col2- 1; b++)
                {
                    Console.Write("Ingrese la casilla [" + a + "] " + "[" + b + "]");
                    matri2[a, b] = int.Parse(Console.ReadLine());
                }
                Console.WriteLine();
            }

            Console.WriteLine("La matriz 1 queda de la siguiente forma: ");
            for (int a = 0; a <= fil - 1; a++)
            {
                for (int b = 0; b <= col - 1; b++)
                {
                    Console.Write(" ["+ matriz[a,b] +"] " );
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            Console.WriteLine("La matriz 2 queda de la siguiente forma: ");
            for (int a = 0; a <= fil2- 1; a++)
            {
                for (int b = 0; b <= col2- 1; b++)
                {
                    Console.Write(" [" + matri2[a, b] + "] ");
                }
                Console.WriteLine();
            }

            matri3 = new int[fil + 1, col2 + 1];
            Console.ReadKey();
            for (int a = 0; a <= fil -1; a++)
            {
                for (int b = 0; b <= col2 -1; b++)
                {
                    matri3[a, b] = 0;
                    for (int c = 0; c <= col -1; c++)
                    {
                        matri3[a, b] = matriz[a, c] * matri2[c, b] + matri3[a, b];
                    }
                }
            }
            Console.WriteLine("Y su multiplicacion es: ");
            for (int a = 0; a <= fil -1; a++)
            {
                for (int b = 0; b <= col2 -1; b++)
                {
                    Console.Write(" ["+ matri3[a,b] +"] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

        public void Autom()
        {
            
        }


        /*
         * 
         * 
        public void Manual()
        {
            Console.WriteLine("M A N U A L");
            Console.WriteLine("Defina las columnas del primer arreglo: ");
            col = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Defina las filas del primer arreglo: ");
            fil = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("El arreglo quedaria de la siguiente manera: ");
            matriz = new int[col, fil];
            char[,] muestra = new char[col, fil];

            for (int a = 0; a <= col- 1; a++)
            {
                for (int b = 0; b <= fil- 1; b++)
                {
                    muestra[a, b] = ast;
                }
            }

            for (int a = 0; a <= col- 1; a++)
            {
                for (int b = 0; b <= fil- 1; b++)
                {
                    Console.Write(" [" + muestra[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

            System.Console.Clear();
            Console.WriteLine("Ingrese la posicion que se le indica: ");
            for (int a = 0; a <= col - 1; a++)
            {
                for (int b = 0; b <= fil- 1; b++)
                {
                    Console.Write("Posicion: [" + a + "] " + " [" + b + "] ");
                    matriz[a, b] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }
            System.Console.Clear();
            Console.WriteLine("La matriz 1 es la siguiente: ");
            for (int a = 0; a <= col - 1; a++)
            {
                for (int b = 0; b <= fil - 1; b++)
                {
                    Console.Write(" [" + matriz[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
            Console.WriteLine("Presione cualquier tecla para continuar");
            System.Console.Clear();

            matri2 = new int[col, fil];
            Console.WriteLine("Comencemos con la segunda matriz: ");
            Console.WriteLine("Ingrese la posicion que se indica: ");
            for (int a = 0; a <= col2 - 1; a++)
            {
                for (int b = 0; b <= fil2 - 1; b++)
                {
                    Console.Write("Posicion: [" + a + "] " + " [" + b + "] ");
                    matri2[a, b] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }

            Console.WriteLine("La primera matriz es la siguiente: ");
            for (int a = 0; a <= col - 1; a++)
            {
                for (int b = 0; b <= fil - 1; b++)
                {
                    Console.Write(" [" + matriz[a, b] + "] ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("La segunda matriz es la siguiente: ");
            for (int a = 0; a <= col - 1; a++)
            {
                for (int b = 0; b <= fil - 1; b++)
                {
                    Console.Write(" [" + matri2[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();          //Aquí hay que modificar.

            matri3 = new int[col, ];
            Console.WriteLine("Y la multiplicación de estos es: ");
            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    matri3[a, b] = matriz[a, b] + matri2[a, b];
                }
            }

            for (int a = 0; a <= tam - 1; a++)
            {
                for (int b = 0; b <= tam - 1; b++)
                {
                    Console.Write(" [" + matri3[a, b] + "] ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();

        }

        public void Autom()
        {

        }*/


        }
    }