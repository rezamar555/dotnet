﻿using System;
using System.Diagnostics;
/*Mauricio Reyes Zamora
Ingenieria en Sistemas Computacionales*/
namespace Matrices
{
    public class ConjuntoMatricial
    {
        public int[,] matriz; //La Diosa Matricial
        const char ast = '*';
        int opc2 = 0, total, buscar;
        int fil,col;
        //M ES EL EQUIVALENTE A FILAS N COLUMNAS
        //bool asg = false;

        public ConjuntoMatricial()
        {
            int opc = 0;

            do
            {

                Console.WriteLine("B U S Q U E D A  B I N A R I A");
                Console.WriteLine("Como deseas crear la matriz?:");
                Console.WriteLine("1.-Manual(Usuario define tamanio, valores y orden");
                Console.WriteLine("2.-Automatico(Usuario define unicamente tamanio y orden)");
                Console.WriteLine("3.-Regresar a Menu Principal");
                //if (!int.TryParse(Console.ReadLine(), out opc)) <<-- Preguntar a Rigo para que funciona
                //System.FormatException has been thrown "Input String was not in a correct format"
                opc = Convert.ToInt32(Console.ReadLine());

                switch (opc)
                {
                    case 1:
                        Manual();
                        break;

                    case 2:
                        Auto();
                        break;

                }

            } while (opc != 3);
        }

        public void Manual()
        {

        }


        public void Auto()
        {
            System.Console.Clear();
            Console.WriteLine("A U T O M A T I C O");
            Console.WriteLine("Defina filas del arreglo:");
            fil = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Defina columnas del arreglo:");
            col = Convert.ToInt32(Console.ReadLine());
            System.Console.Clear();
            Console.WriteLine("A U T O M A T I C O");
            //Console.WriteLine("Define el numero de filas:");
            //fil = Convert.ToInt32(Console.ReadLine());
            matriz = new int[fil, col]; //Se usara para el orden
            char[,] veces = new char[fil, col]; //Se usa para el muestreo
            //System.Console.Clear();
            //Console.WriteLine("Tu matriz es de columnas: " + col);
            //Console.WriteLine("Tu matriz es de filas: " + fil);
            for (int a = 0; a <= fil - 1; a++)
            {
                for (int b = 0; b <= col - 1; b++)
                {
                    veces[a, b] = ast;
                    //matriz[a, b] = ast;
                    //Console.Write("*",matriz[col,fil]);
                }

            }
            //Console.WriteLine("A U T O M A T I C O");
            Console.WriteLine("Dimensiones de la matriz:");

            for (int a = 0; a <= fil - 1; a++)
            {
                for (int b = 0; b <= col - 1; b++)
                {
                    //Console.Write(veces["[" +a+ "]","[" +b+ "]"]);
                    Console.Write(" [" + veces[a, b] + "] ");

                }
                Console.WriteLine();
            }
            //System.Console.Cle
            Console.WriteLine("Es Correcta la matriz?");
            Console.WriteLine("1.-No");
            Console.WriteLine("2.-Si");
            opc2 = Convert.ToInt32(Console.ReadLine());

            if (opc2 == 1)
            {
                Console.WriteLine("De acuerdo, intentemos de nuevo... Presiona cualquier telca");
                Console.ReadKey();
                Auto();
            }
            else if (opc2 == 2)
            {
                int opc3 = 0;
                Console.WriteLine("A U T O M A T I C O");
                Console.WriteLine("De acuerdo, ahora escoge el orden de los elementos:");
                Console.WriteLine("1.-Ascendente(1,n+1)");
                Console.WriteLine("2.-Descendente(n-1,1)");
                opc3 = Convert.ToInt32(Console.ReadLine());

                switch (opc3)
                {
                    case 1:
                        System.Console.Clear();
                        Console.WriteLine("A U T O M A T I C O");
                        Console.WriteLine("Haz escogido rellenar la matriz de forma automatica y ascendente");
                        Console.WriteLine("La matriz queda de la siguiente forma: ");
                        total = 1;
                        for (int a = 0; a <= fil - 1; a++)
                        {
                            for (int b = 0; b <= col - 1; b++)
                            {
                                //total = total + total; 
                                matriz[a, b] = total++;
                                Console.Write(" [" + matriz[a, b] + "] ");
                                //total = total + veces[a, b];
                                //Console.WriteLine(veces[a,b]);
                            }
                            Console.WriteLine();
                        }
                        Console.WriteLine("Que elemento desea buscar?");
                        buscar = Convert.ToInt32(Console.ReadLine());
                        Buscarelemento(matriz, buscar);

                        void Buscarelemento(int [,]matriz, int buscar)
                        {
                            for (int m = 0; m < fil; ++m)
                            {
                                int n = Busquedabinaria(matriz, m, buscar);
                                if (n > -1)
                                {
                                    Console.WriteLine("El elemento " + buscar + " se encuentra en [" + m + "][" + n + "]");
                                }
                            }
                        }

                        int Busquedabinaria(int [,] matriz,int m,int buscar)
                        {
                            int inicio = 0;
                            int final = col - 1;
                            //bool encontrado;
                            while(inicio < final)
                            {
                                int mitad = (final - inicio) / 2 + inicio;
                                //if(buscar < matriz[m, mitad] && encontrado == false)
                                if (buscar < matriz[m, mitad])
                                {
                                    final = mitad - 1;
                                }
                                else if (buscar > matriz[m, mitad])
                                {
                                    inicio = mitad + 1;
                                }
                                else
                                {
                                    inicio = mitad;
                                    break;
                                }
                            }
                            if (matriz[m,inicio] == buscar)
                                return inicio;
                            return -1;
                        }
                        Console.ReadKey();
                        break;

                    case 2:
                        //Este pedazo de mierda no
                        break;
                }
            }
        }
    }
}